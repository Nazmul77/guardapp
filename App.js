

import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'

import LoadingScreen from './screens/LoadingScreen'
import RegisterScreen from './screens/RegisterScreen'

import HomeScreen from './screens/HomeScreen'


import LoginScreen from './screens/LoginScreen'

import * as firebase from 'firebase'


var firebaseConfig = {
  apiKey: 'AIzaSyDYO4kCYAeoKLDtshzV14MChJlUH-gaaEo',
  authDomain: 'guardprojectdemo.firebaseapp.com',
  databaseURL: 'https://guardprojectdemo.firebaseio.com',
  projectId: 'guardprojectdemo',
  storageBucket: 'guardprojectdemo.appspot.com',
  messagingSenderId: '751058343811',
  appId: '1:751058343811:web:ca76767ff0291b571488b6',
  measurementId: 'G-DCVBDDDWX1',
};
// Initialize Firebase

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
  


}




const AppStack =createStackNavigator({

Home: HomeScreen

})

const AuthStack= createStackNavigator(
  // Login: LoginScreen,
  // Resigter: RegisterScreen,



  {
    Login: {
        screen: LoginScreen
    },
    Register: {
        screen: RegisterScreen
    },
    

},

{
    mode: 'modal'
}
);




export default createAppContainer(
createSwitchNavigator(
  {
    Loading: LoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: "Loading"
  }
)

  
);




