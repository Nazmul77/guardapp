/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import * as firebase from 'firebase';

// create a component
export default class HomeScreen extends React.Component {
  state = {
    email: '',
    displayName: '',
    companyName: '',
    phoneNumber: '',
    id: '',
    nidNumber: ''

  };


  componentDidMount() {
    
    firebase
      .database()
      .ref('users')
      .on('child_added', data => {
          // alert(JSON.stringify(data));
          data=JSON.stringify(data)
          data=JSON.parse(data)
        const companyName = data.companyName;
        const phoneNumber= data.phoneNumber;
        const nidNumber=data.nidNumber;
        const id =data.id;
        // alert(companyName)
        // const companyName = data.companyName;
        // this.setState({companyName})
        this.setState({
          companyName: companyName,
phoneNumber: phoneNumber,
id: id,
nidNumber: nidNumber
        });

        // alert(this.setState.companyName)
      });

    const {email, displayName} = firebase.auth().currentUser;
    // eslint-disable-next-line no-unused-vars
    const currentUser = firebase.auth().currentUser;
    // alert(JSON.stringify(currentUser));
    this.setState({email, displayName});
  }
  signOutUser = () => {
    firebase.auth().signOut();
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Email:          {this.state.email}</Text>
        <Text>Name:           {this.state.displayName}</Text>
        <Text>Company Name:   {this.state.companyName}</Text>
        <Text>Nid Number:     {this.state.nidNumber}</Text>
        <Text>Id Number:      {this.state.id}</Text>

        <TouchableOpacity style={{marginTop: 32}} onPress={this.signOutUser}>
          <Text>LogOut</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    textAlign: "center",
    justifyContent: 'center',
  },
});
