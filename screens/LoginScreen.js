//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput,TouchableOpacity,Image, StatusBar,LayoutAnimation } from 'react-native';
import * as firebase from 'firebase'

// create a component
export default  class LoginScreen extends React.Component {
static navigationOption={
    header: null
}


    state={
        email: "",
        password: "",
        errorMessage: null
    };

    handleLogin=() =>{
        const{email,password}=this.state;
        firebase
        .auth().signInWithEmailAndPassword(email,password)
        .catch(error=>this.setState({errorMessage: error.message}))
    };

    render() {
        return (
            <View style={styles.container}>

                <StatusBar barStyle="light-content"></StatusBar>
                <Text style={styles.greeting}>{`Hellow again.\nWelcome back.` }</Text>


                <View style={styles.errorMessage}>

{this.state.errorMessage && <Text style ={styles.error}>{this.state.errorMessage}</Text>}


</View>
<View style={styles.form}>

    <View>
        <Text style={styles.inputTittle}>Email Address</Text>
        <TextInput style={styles.input}  autoCapitalize="none" 
        onChangeText={email=>this.setState({email})}
         value={this.state.email}
        ></TextInput>
    </View>

    <View style={{marginTop: 32}}>
        <Text style={styles.inputTittle}>Password</Text>
        <TextInput style={styles.input}  autoCapitalize="none"  secureTextEntry autoCapitalize="none" 
        onChangeText={password=>this.setState({password})}
        value={this.state.password}
        ></TextInput>
    </View>
</View>





<TouchableOpacity style={styles.button}  onPress={this.handleLogin}>
    <Text style={{color: "#FFF", fontWeight: "500"}}>SingIn</Text>
</TouchableOpacity>


<TouchableOpacity style={{alignSelf:  "center", marginTop: 32}} 
onPress={()=> this.props.navigation.navigate('Register')}>

    <Text style={{color: "#414959", fontSize: 13}}>New to SocialApp?
     <Text style={{ fontWeight: "500", color: "#E9446A"}}> SingUp</Text></Text>
</TouchableOpacity>



            </View>

          
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
       
    },

    greeting: {
        marginTop: 32,
        fontSize: 18,
        fontWeight: "400",
        textAlign: "center"
    },
    errorMessage: {
        height: 72,
        alignItems: "center",
        marginHorizontal: 30
    },
    form: {
        marginBottom: 48,
        marginHorizontal: 30,
    },
    inputTittle: {
        color: "#8A8F9E",
        fontSize: 10,
        textTransform: "uppercase",

    },
    input: {
        borderBottomColor: "#8A8F9E",
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 40,
        fontSize: 15,
        color: "#161F3D"
    },
    button: {
        marginHorizontal: 30,
        backgroundColor: "#E9446A",
        borderRadius: 4,
        height: 52,
        alignItems: "center",
        justifyContent: "center",


    },
    error: {
       color:  "#E9446A",
       fontSize: 13,
       fontWeight: "600",
       textAlign: "center"
    }

});
