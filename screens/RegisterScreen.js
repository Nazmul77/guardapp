/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import * as firebase from 'firebase';

// create a component
export default class RegisterScreen extends React.Component {
 

  
  state = {
    name: '',
    email: '',
    companyName: '',
phoneNumber: '',
id: '',
nidNumber: '',

    password: '',
    errorMessage: null,
  };

  // onChangeTextlistner(companyName) {
  //   console.log('onChangeTextlistner');

  //   this.setState({companyName});
  //   // this.setState({id});
  //   // this.setState({phoneNumber});
  // }

  handleSignUp = (companyName,phoneNumber,id,nidNumber,name) => {
    firebase.database().ref('users/').push({
      companyName,
      phoneNumber,
      nidNumber,
      id,
      name
      
    }).then((data)=>{
      console.log('data',data)
    }).catch((error)=>{
      console.log('error',error)
    })
  

    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(userCredentials => {
        return userCredentials.user.updateProfile({
          displayName: this.state.name,
        });
      })
      .catch(error => this.setState({errorMessage: error.message}));
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Text
            // eslint-disable-next-line quotes
            style={styles.greeting}>{`Hello!\nSign up to get started.`}</Text>

          <View style={styles.errorMessage}>
            {this.state.errorMessage && (
              <Text style={styles.error}>{this.state.errorMessage}</Text>
            )}
          </View>

          <View style={styles.form}>
            <View>
              <Text style={styles.inputTittle}>Full Name</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={name => this.setState({name})}
                value={this.state.name}
              />
            </View>

          

          <View>
              <Text style={styles.inputTittle}>Company Name</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={companyName => this.setState({companyName})}
                value={this.state.companyName}
              />
            </View>


            <View>
              <Text style={styles.inputTittle}>phone Number</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={phoneNumber => this.setState({phoneNumber})}
                value={this.state.phoneNumber}
              />
            </View>

            <View>
              <Text style={styles.inputTittle}>Id</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={id => this.setState({id})}
                value={this.state.id}
              />
            </View>

            <View>
              <Text style={styles.inputTittle}>nidNumber</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={nidNumber => this.setState({nidNumber})}
                value={this.state.nidNumber}
              />
            </View>


            {/* <View style={{marginTop: 32}}>
        <Text style={styles.inputTittle}>Phone Number</Text>
        // eslint-disable-next-line no-trailing-spaces
        <TextInput style={styles.input}  autoCapitalize="none"

        onChangeText={phoneNumber=>this.onChangeTextlistner(phoneNumber)}
         value={this.state.phoneNumber}
        ></TextInput>
    </View> */}

            {/*

    <View style={{marginTop: 32}}>
        <Text style={styles.inputTittle}>Id Number</Text>
        // eslint-disable-next-line no-trailing-spaces
        <TextInput style={styles.input}  autoCapitalize="none"

        onChangeText={id=>this.onChangeTextlistner(id)}
         value={this.state.id}
        ></TextInput>
    </View> */}


    <View style={{marginTop: 32}}>
              <Text style={styles.inputTittle}>Email Address</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={email => this.setState({email})}
                value={this.state.email}
              />
            </View>


            <View style={{marginTop: 32}}>
              <Text style={styles.inputTittle}>Password</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                secureTextEntry
                autoCapitalize="none"
                onChangeText={password => this.setState({password})}
                value={this.state.password}
              />
            </View>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.handleSignUp(this.state.companyName,this.state.phoneNumber,this.state.nidNumber,this.state.id,this.state.name)}>
            <Text style={{color: '#FFF', fontWeight: '500'}}>SingUp</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{alignSelf: 'center', marginTop: 32}}>
            <Text style={{color: '#414959', fontSize: 13}}>
              New to SocialApp?
              <Text style={{fontWeight: '500', color: '#E9446A'}}> Login</Text>
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  greeting: {
    marginTop: 32,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    marginHorizontal: 30,
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTittle: {
    color: '#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#8A8F9E',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#161F3D',
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#E9446A',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
  error: {
    color: '#E9446A',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  scrollView: {
    backgroundColor: 'pink',
    marginHorizontal: 20,
  },
});
